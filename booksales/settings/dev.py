from .prod import *  # noqa
from .prod import INSTALLED_APPS, MIDDLEWARE

DEBUG = True
INSTALLED_APPS += ["debug_toolbar"]
MIDDLEWARE.insert(1, "debug_toolbar.middleware.DebugToolbarMiddleware")

INTERNAL_IPS = [
    "127.0.0.1",
]

SHELL_PLUS_IMPORTS = [
    "from django.utils.text import slugify",
    "from django.core.serializers import serialize",
    "import io",
    "import os",
    "import sys",
    "import csv",
    "import json",
    "import uuid",
    "import random",
    "import secrets",
    "import datetime",
    "from uuid import UUID",
    "from decimal import Decimal",
    "from faker import Faker",
]
