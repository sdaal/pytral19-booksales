from random import SystemRandom

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from faker import Faker

random = SystemRandom()
User = get_user_model()


class Command(BaseCommand):
    help = "Create random users"

    def add_arguments(self, parser):
        parser.add_argument('-c',
                            '--count',
                            type=int,
                            default=10,
                            help='How many users will be created')

    def handle(self, **options):
        fake = Faker()
        users = []
        for i in range(options['count']):
            name = fake.first_name()
            surname = fake.last_name()
            username = f'{name.lower()}_{surname.lower()}_{random.randrange(1, 1000)}'
            email = '{}@{}'.format(username, fake.domain_name())
            user = User(
                first_name=name,
                last_name=surname,
                username=username,
                email=email,
                is_active=True,
                is_staff=False,
                is_superuser=False,
                phone=fake.phone_number(),
            )
            user.set_password('pass123')
            users.append(user)
        User.objects.bulk_create(users)
