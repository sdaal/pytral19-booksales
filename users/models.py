from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    phone = models.CharField(_('phone number'), max_length=32, blank=True)

    class Meta(AbstractUser.Meta):
        db_table = 'users'
        verbose_name = _('user')
        verbose_name_plural = _('users')
