from django.contrib.gis import admin

from .models import Book


@admin.register(Book)
class BookAdmin(admin.GISModelAdmin):
    list_display = ('name', 'title', 'price', 'location',)
    search_fields = ('name', 'title',)
    list_filter = ('price',)
    fieldsets = [
        ("Post", {
            "fields": ("name", "price",)
        }),
        ("Book", {
            "fields": ("title", "author", "description", "cover",),
        }),
        ("Seller", {
            "fields": ("user", "location",),
        })
    ]
