import io
import random

# from django.contrib.auth.models import User   # not a good practice
from django.contrib.auth import get_user_model  # suggested way to get the user model
from django.core.files.images import ImageFile
from django.core.management.base import BaseCommand
from faker import Faker

from books.models import Book

User = get_user_model()


class Command(BaseCommand):
    help = "Create random books"

    def add_arguments(self, parser):
        parser.add_argument('-c',
                            '--count',
                            type=int,
                            default=10,
                            help='How many books will be created')

    def handle(self, **options):
        user_ids = User.objects.values_list('id', flat=True)
        fake = Faker()
        books = []
        for i in range(options['count']):
            book = Book(
                name=fake.sentence(),
                title=fake.bs().title(),
                author=fake.name(),
                description='\n'.join(fake.sentences()),
                price=random.randrange(1, 51) * 100,
                user_id=random.choice(user_ids),
                cover=ImageFile(io.BytesIO(fake.image(size=(640, 480))),
                                fake.file_name(extension='png')),
            )
            print("Creating book:", book)
            books.append(book)
        Book.objects.bulk_create(books)
