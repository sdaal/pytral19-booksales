from django.conf import settings
from django.contrib.gis.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill


class Book(models.Model):
    name = models.CharField(_("name"), max_length=256)
    title = models.CharField(_("title"), max_length=128)
    author = models.CharField(_("author"), max_length=256)
    description = models.TextField(_("description"), blank=True)
    price = models.IntegerField(_("price"))
    cover = models.ImageField(_("cover"), upload_to='books', blank=True, null=True)
    cover_thumb = ImageSpecField(
        source='cover',
        processors=[ResizeToFill(90, 120)],
        format='JPEG',
        options={'quality': 80},
    )
    cover_sm = ImageSpecField(
        source='cover',
        processors=[ResizeToFill(180, 240)],
        format='JPEG',
        options={'quality': 80},
    )
    cover_md = ImageSpecField(
        source='cover',
        processors=[ResizeToFill(360, 480)],
        format='JPEG',
        options={'quality': 70},
    )
    cover_lg = ImageSpecField(
        source='cover',
        processors=[ResizeToFill(480, 640)],
    )
    is_active = models.BooleanField(_("active"), default=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    location = models.PointField(null=True, blank=True)
    created_at = models.DateTimeField(_("created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("updated at"), auto_now=True)

    class Meta:
        db_table = 'books'
        verbose_name = _("book")
        verbose_name_plural = _("books")
        ordering = ['-created_at']
        get_latest_by = 'created_at'

    def __str__(self):
        return f'{self.title} ({self.author})'

    def get_absolute_url(self):
        return reverse('book_detail', kwargs={'pk': self.pk})
