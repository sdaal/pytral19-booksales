from django import template

register = template.Library()


@register.filter
def plusify(value):
    if value.startswith("00"):
        return f'+{value.removeprefix("00")}'
    return value
