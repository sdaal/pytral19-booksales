from django.urls import path

from . import views

urlpatterns = [
    path("books/add/", views.BookCreateView.as_view(), name="book_add"),
    path("books/<int:pk>/", views.BookDetailView.as_view(), name="book_detail"),
    path("books/<int:pk>/edit/", views.BookUpdateView.as_view(), name="book_edit"),
    path("books/<int:pk>/delete/", views.BookDeleteView.as_view(), name="book_delete"),
    path("books/", views.BookListView.as_view(), name="book_list"),
    path("", views.BookListView.as_view(), name="home"),
    path("users/<int:user_id>/books/", views.BookListView.as_view(), name="user_books"),
    path("p/pub-key/", views.get_pub_key, name="pub_key"),
    path("p/success/", views.payment_success, name="payment_success"),
    path("p/cancel/", views.payment_canceled, name="payment_cancel"),
    path("p/session/<int:pk>/", views.get_checkout_session, name="checkout_session"),
    path("map/", views.BookMapView.as_view(), name="book_map"),
]
