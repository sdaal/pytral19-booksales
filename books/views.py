import json
import logging
from typing import Any

import stripe
from django.conf import settings
from django.core import serializers
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from .forms import BookForm
from .models import Book

logger = logging.getLogger(__name__)


class BookCreateView(LoginRequiredMixin, generic.CreateView):
    model = Book
    form_class = BookForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()
        return super().form_valid(form)


class BookListView(generic.ListView):
    paginate_by = 20

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        paginator = context["paginator"]
        page_obj = context["page_obj"]
        pages = paginator.get_elided_page_range(
            page_obj.number, on_each_side=2, on_ends=1
        )
        context.update({"pages": pages})
        return context

    def get_queryset(self):
        qs = Book.objects.order_by("-pk").all()
        user_id = self.kwargs.get("user_id")
        if user_id is not None:
            qs = qs.filter(user_id=user_id)
        return qs


class BookDetailView(generic.DetailView):
    model = Book


class BookUpdateView(LoginRequiredMixin, UserPassesTestMixin, generic.UpdateView):
    model = Book
    form_class = BookForm

    def test_func(self):
        book = self.get_object()
        return self.request.user == book.user


class BookDeleteView(LoginRequiredMixin, UserPassesTestMixin, generic.DeleteView):
    model = Book
    success_url = reverse_lazy("home")

    def test_func(self):
        book = self.get_object()
        return self.request.user == book.user


@csrf_exempt
def get_pub_key(request):
    return JsonResponse(
        {
            "publicKey": settings.STRIPE_PUBLISHABLE_KEY,
        },
        safe=False,
    )


@csrf_exempt
def get_checkout_session(request, pk):
    book = Book.objects.get(pk=pk)
    domain_url = "{}://{}".format(request.scheme, request.get_host())
    stripe.api_key = settings.STRIPE_SECRET_KEY
    try:
        checkout_session = stripe.checkout.Session.create(
            success_url=f"{domain_url}{reverse('payment_success')}",
            cancel_url=f"{domain_url}{reverse('payment_cancel')}",
            payment_method_types=["card"],
            mode="payment",
            line_items=[
                {
                    "price_data": {
                        "currency": "all",
                        "unit_amount": str(book.price * 100),
                        "product_data": {
                            "name": book.name,
                            "description": book.description,
                            "images": [f"{domain_url}{book.cover.url}"],
                        },
                    },
                    "quantity": 1,
                }
            ],
        )
        return JsonResponse({"sessionId": checkout_session["id"]})
    except Exception as e:
        logger.exception("Could not create checkout session: %s", exc_info=True)
        return JsonResponse({"error": str(e)}, status=400)


def payment_success(request):
    return render(request, "books/payment_success.html")


def payment_canceled(request):
    return render(request, "books/payment_canceled.html")


class BookMapView(generic.TemplateView):
    template_name = "books/book_map.html"

    def get_context_data(self, **kwargs) -> dict[str, Any]:
        context = super().get_context_data(**kwargs)
        markers = json.loads(
            serializers.serialize(
                "geojson",
                Book.objects.filter(location__isnull=False).all(),
                geometry_field="location",
            )
        )

        for feature in markers["features"]:
            feature["properties"]["url"] = reverse("book_detail", kwargs={"pk": feature["id"]})

        context["markers"] = markers
        return context
