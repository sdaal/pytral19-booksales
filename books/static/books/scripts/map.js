(function (window, document, undefined) {
  "use strict";

  window.onload = function () {
    const copy =
      "&copy; <a href='https://www.openstreetmap.org/copyright'>OpenStreetMap</a>";
    const url = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
    const layer = L.tileLayer(url, {
      attribution: copy,
    });
    const map = L.map("map", {
      layers: [layer],
    });

    const markersDataElem = document.querySelector("#markers-data");
    const markers = JSON.parse(markersDataElem.textContent);
    console.log(JSON.stringify(markers, null, 2));
    let feature = L.geoJSON(markers)
      .bindPopup(function (layer) {
        return `<p><a href="${layer.feature.properties.url}">${layer.feature.properties.name}</a><br><b>${layer.feature.properties.title}</b> by <i>${layer.feature.properties.author}</i></p>`;
      })
      .addTo(map);
    map.fitBounds(feature.getBounds(), {
      padding: [100, 100],
    });
  };
})(window, document);
