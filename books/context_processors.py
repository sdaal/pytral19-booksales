import os

from django.urls import reverse

from books.models import Book


def sales(request):
    directory = os.path.dirname(__file__)
    sales_file = os.path.join(directory, 'sales.txt')
    discounts = {}
    with open(sales_file) as f:
        for line in f:
            parts = [int(p) for p in line.strip().split("|")]
            discounts[parts[0]] = parts[1]
    books = Book.objects.filter(pk__in=discounts.keys()).all()

    messages = []
    for book in books:
        pct = discounts[book.pk]
        messages.append({
            "url": reverse("book_detail", kwargs={"pk": book.pk}),
            "title": f"{pct}% discount on {book.title}",
        })

    return {"sales": messages}
