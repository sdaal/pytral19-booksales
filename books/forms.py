from django import forms

from .models import Book


class BookFormOld(forms.Form):
    name = forms.CharField(required=True, max_length=256)
    title = forms.CharField(required=True, max_length=128)
    author = forms.CharField(required=True, max_length=256)
    description = forms.CharField(required=True, widget=forms.Textarea)
    price = forms.IntegerField(required=True, min_value=0, max_value=50_000)
    seller = forms.CharField(required=True, max_length=128)
    cover = forms.ImageField(required=False)


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            'name',
            'title',
            'author',
            'description',
            'price',
            'cover',
        ]
