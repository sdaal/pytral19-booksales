from django.urls import path

from . import views

urlpatterns = [
    path("about-us/", views.AboutView.as_view(), name="about"),
    path("contact-us/", views.ContactView.as_view(), name="contact"),
    path("privacy-policy/", views.PrivacyView.as_view(), name="privacy"),
    path("terms/", views.TermsView.as_view(), name="terms"),
]
